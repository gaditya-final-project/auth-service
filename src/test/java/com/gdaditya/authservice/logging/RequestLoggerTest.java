package com.gdaditya.authservice.logging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ExtendWith(SpringExtension.class)
public class RequestLoggerTest {

    @Mock
    LogService logService;

    @InjectMocks
    RequestLogger requestLogger;

    HttpServletRequest httpServletRequest;

    HttpServletResponse httpServletResponse;

    FilterChain filterChain;

    @BeforeEach
    public void setup() {
        httpServletRequest = Mockito.mock(HttpServletRequest.class);
        httpServletResponse = Mockito.mock(HttpServletResponse.class);
        filterChain = Mockito.mock(FilterChain.class);

        Mockito.when(httpServletRequest.getRemoteAddr()).thenReturn("1");
        Mockito.when(httpServletRequest.getRequestURI()).thenReturn("2");
        Mockito.when(httpServletRequest.getMethod()).thenReturn("3");


    }

    @Test
    public void whenFilter_logOnce() throws ServletException, IOException {
        requestLogger.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(logService, Mockito.times(1)).logRequest("1", "2", "3");
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(httpServletRequest, httpServletResponse);
    }

}
