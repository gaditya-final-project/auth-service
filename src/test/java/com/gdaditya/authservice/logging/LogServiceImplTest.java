package com.gdaditya.authservice.logging;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
public class LogServiceImplTest {

    @Mock
    KafkaTemplate<String, String> kafkaTemplate;

    @InjectMocks
    LogServiceImpl logService;

    @BeforeEach
    public  void setup() {
        ReflectionTestUtils.setField(logService, "serviceName", "test-service");
    }

    @Test
    public void whenLog_shouldProduceAsFormatted() {
        logService.logRequest("1", "2", "3");
        Mockito.verify(kafkaTemplate, Mockito.times(1)).send("log",
                String.format("%s: request from %s to %s method %s.", "test-service", "1", "2", "3"));
    }


}
