package com.gdaditya.authservice.service;

import com.gdaditya.authservice.exception.BaseException;
import com.gdaditya.authservice.exception.WrongUsernameOrPassword;
import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.model.dto.TokenObject;
import com.gdaditya.authservice.model.dto.UserLoginInput;
import com.gdaditya.authservice.repository.UserRepository;
import com.gdaditya.authservice.service.impl.AuthServiceImpl;
import com.gdaditya.authservice.util.JwtTokenProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AuthServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    AuthServiceImpl authService;

    TokenObject tokenObject;

    Authentication authentication;

    UserLoginInput userLoginInput;

    @BeforeEach
    public void setup() {
        tokenObject = new TokenObject("Bearer 123", new Date());
        User user = new User();
        userLoginInput = new UserLoginInput();
        userLoginInput.setUsername("test-user");
        userLoginInput.setPassword("123");
        authentication = Mockito.mock(Authentication.class);
        Mockito.when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(authentication);
        Mockito.when(userRepository.findUserByUsername(userLoginInput.getUsername()))
                .thenReturn(user);
        Mockito.when(jwtTokenProvider.generateToken(user)).thenReturn(tokenObject);
    }

    @Test
    public void whenAuthenticated_shouldReturnToken() throws BaseException {
        Mockito.when(authentication.isAuthenticated()).thenReturn(true);

        TokenObject tokenObjectRes = authService.generateToken(userLoginInput);

        Assertions.assertEquals(tokenObjectRes, tokenObject);
    }

    @Test
    public void whenAuthenticationFail_shouldThrowWrongUsernameOrPassword() {
        Mockito.when(authentication.isAuthenticated()).thenReturn(false);

        Assertions.assertThrows(WrongUsernameOrPassword.class, () -> {
            authService.generateToken(userLoginInput);
        });
    }

}
