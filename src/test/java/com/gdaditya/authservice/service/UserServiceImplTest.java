package com.gdaditya.authservice.service;

import com.gdaditya.authservice.exception.*;
import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.model.UserProfile;
import com.gdaditya.authservice.model.UserRole;
import com.gdaditya.authservice.model.dto.UserCreateInput;
import com.gdaditya.authservice.model.dto.UserUpdateInput;
import com.gdaditya.authservice.repository.UserProfileRepository;
import com.gdaditya.authservice.repository.UserRepository;
import com.gdaditya.authservice.repository.UserRoleRepository;
import com.gdaditya.authservice.service.impl.UserServiceImpl;
import com.gdaditya.authservice.util.JwtTokenProvider;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserServiceImplTest {

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserRepository userRepository;

    @Mock
    UserProfileRepository userProfileRepository;

    @Mock
    UserRoleRepository userRoleRepository;

    @Mock
    JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    UserServiceImpl userService;

    User user1;

    UserProfile userProfile1;

    UserUpdateInput userUpdateInput1;

    UserCreateInput userCreateInput1;

    Collection<User> users;

    String user1Token;

    EasyRandom generator = new EasyRandom();

    @BeforeEach
    public void setup() throws JwtTokenMalformedException, JwtTokenMissingException {
        Mockito.when(passwordEncoder.encode(Mockito.any(String.class))).thenAnswer(i -> i.getArguments()[0]);

        userProfile1 = generator.nextObject(UserProfile.class);
        user1 = User.builder()
                .id((long)1)
                .username("user1")
                .password("123")
                .email("email@mail.com")
                .role(new UserRole("TRAINER"))
                .userProfile(userProfile1).build();
        users = List.of(user1);
        userCreateInput1 = new UserCreateInput();
        userCreateInput1.setUsername(user1.getUsername());
        userCreateInput1.setEmail(user1.getEmail());
        userCreateInput1.setPassword(user1.getPassword());
        userCreateInput1.setRole(user1.getRole().getName());
        userCreateInput1.setFirstName(user1.getUserProfile().getFirstName());
        userCreateInput1.setLastName(user1.getUserProfile().getLastName());
        userCreateInput1.setProfilePictureLink(user1.getUserProfile().getProfilePictureLink());

        userUpdateInput1 = new UserUpdateInput();
        userUpdateInput1.setId((long)1);
        userUpdateInput1.setUsername("Yae Miko");
        userUpdateInput1.setPassword("B4alCryB@by");
        userUpdateInput1.setEmail("yae.miko@inazuma.com");
        userUpdateInput1.setRole("TRAINER");
        userUpdateInput1.setFirstName("Yae");
        userUpdateInput1.setLastName("Miko");
        userUpdateInput1.setProfilePictureLink("https://static.zerochan.net/Yae.Miko.full.3390776.png");



        user1Token = "Bearer abc";

        Mockito.when(jwtTokenProvider.getUserId(user1Token)).thenReturn((long)1);
        Mockito.when(userRepository.findUserByUsername(user1.getUsername())).thenReturn(user1);
        Mockito.when(userRepository.findAll()).thenReturn((List<User>) users);
        Mockito.when(userRepository.findUserById(user1.getId())).thenReturn(user1);
        Mockito.when(userRepository.save(Mockito.any())).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(userRoleRepository.findUserRoleByName("TRAINER")).thenReturn(new UserRole("TRAINER"));
    }

    @Test
    public void whenLoadExistingUserByUsername_shouldReturnUserDetails() {
        User userRes = (User) userService.loadUserByUsername(user1.getUsername());
        Assertions.assertEquals(user1, userRes);
    }

    @Test
    public void whenLoadNonexistentUserByUsername_shouldThrowWrongUsernameOrPassword() {
        Assertions.assertThrows(WrongUsernameOrPassword.class, () -> {
            userService.loadUserByUsername("Sosig");
        });
    }

    @Test
    public void whenGetAllUser_shouldReturnUsersList() {
        Collection<User> usersRes = userService.getAllUser();
        Assertions.assertEquals(users, usersRes);
    }

    @Test
    public void whenGetFromIdExists_shouldReturnUser() throws UserNotFoundException {
        User userRes = userService.getFromId((long)1);
        Assertions.assertEquals(user1, userRes);
    }

    @Test
    public void whenGetFromIdNonexistent_shouldThrowUserNotFound()  {
        Assertions.assertThrows(UserNotFoundException.class, () -> userService.getFromId((long)-1));
    }

    @Test
    public void whenGetFromTokenExists_shouldReturnUser() throws JwtTokenMalformedException, JwtTokenMissingException {
        User userRes = userService.getFromToken(user1Token);
        Assertions.assertEquals(user1, userRes);
    }

    @Test
    public void whenGetFromTokenNonexistent_shouldThrowJwtTokenMalformed() {
        Assertions.assertThrows(JwtTokenMalformedException.class, () -> userService.getFromToken("Yae huha"));
    }

    @Test
    public void whenUpdateUserSuccess_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateNoID_shouldReturnUpdateSelf() throws JwtTokenMalformedException, JwtTokenMissingException, UsernameAlreadyExistsException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("TRAINER");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)1);

        userUpdateInput1.setId(null);
        userUpdateInput1.setRole(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateIDSelf_shouldReturnUpdateSelf() throws JwtTokenMalformedException, JwtTokenMissingException, UsernameAlreadyExistsException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("TRAINER");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)1);

        userUpdateInput1.setId((long)1);
        userUpdateInput1.setRole(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoUsername_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setUsername(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(user1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoPassword_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setPassword(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(user1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoEmail_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setEmail(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(user1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoRole_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setRole(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(user1.getRole().getName(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoFirstName_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setFirstName(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userProfile1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoLastName_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setLastName(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userProfile1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userUpdateInput1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateUserSuccessNoProfilePictureLink_shouldReturnUpdatedUser() throws UsernameAlreadyExistsException, JwtTokenMalformedException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);
        userUpdateInput1.setProfilePictureLink(null);

        User userRes = userService.updateUser(userUpdateInput1, "Bearer abc");

        Assertions.assertEquals(userUpdateInput1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(userUpdateInput1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(userUpdateInput1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(userUpdateInput1.getRole(), userRes.getRole().getName());
        Assertions.assertEquals(userUpdateInput1.getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(userUpdateInput1.getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(userProfile1.getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenUpdateNotAdmin_shouldThrowUpdateOtherUserUnauthorized() throws JwtTokenMalformedException, JwtTokenMissingException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("TRAINER");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        Assertions.assertThrows(UpdateOtherUserUnauthorizedException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void whenUpdateUserNotFound_shouldThrowUserNotFound() throws JwtTokenMalformedException, JwtTokenMissingException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        Mockito.when(userRepository.findUserById(userUpdateInput1.getId())).thenReturn(null);

        Assertions.assertThrows(UserNotExistsException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void whenUpdateUsernameTaken_shouldThrowUsernameAlreadyExists() throws JwtTokenMalformedException, JwtTokenMissingException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        Mockito.when(userRepository.findUserByUsername(userUpdateInput1.getUsername())).thenReturn(user1);

        Assertions.assertThrows(UsernameAlreadyExistsException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void whenUpdateEmailRegistered_shouldThrowEmailAlreadyExists() throws JwtTokenMalformedException, JwtTokenMissingException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        Mockito.when(userRepository.findUserByEmail(userUpdateInput1.getEmail())).thenReturn(user1);

        Assertions.assertThrows(EmailAlreadyExistsException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void whenUpdateSelfRole_shouldRThrowUpdateRoleUnauthorizedException() throws JwtTokenMalformedException, JwtTokenMissingException, UsernameAlreadyExistsException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("TRAINER");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)1);

        Assertions.assertThrows(UpdateRoleUnauthorizedException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void whenUpdateRoleNotExistent_shouldRThrowRoleNotExistsException() throws JwtTokenMalformedException, JwtTokenMissingException, UsernameAlreadyExistsException, UserNotExistsException, RoleNotExistsException, EmailAlreadyExistsException, UpdateOtherUserUnauthorizedException, UpdateRoleUnauthorizedException {
        Mockito.when(jwtTokenProvider.getUserRole(Mockito.any(String.class))).thenReturn("ADMIN");
        Mockito.when(jwtTokenProvider.getUserId(Mockito.any(String.class))).thenReturn((long)2);

        Mockito.when(userRoleRepository.findUserRoleByName(userUpdateInput1.getRole())).thenReturn(null);

        Assertions.assertThrows(RoleNotExistsException.class, () -> userService.updateUser(userUpdateInput1, "Bearer abc"));
    }

    @Test
    public void wheDeleteUserExists_shouldCallRepositoryDelete() throws UserNotExistsException {
        userService.deleteUser(user1.getId());
        Mockito.verify(userRepository, Mockito.times(1)).delete(user1);
    }

    @Test
    public void wheDeleteUserNonexistent_shouldThrowUserNotExistsException() throws UserNotExistsException {
        Mockito.when(userRepository.findUserById(user1.getId())).thenReturn(null);
        Assertions.assertThrows(UserNotExistsException.class, () -> userService.deleteUser(user1.getId()));
    }

    @Test
    public void whenCreateUserSuccess_shouldReturnUser() throws BaseException {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);


        User userRes = userService.createUser(userCreateInput1);

        Assertions.assertEquals(user1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(user1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(user1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(user1.getRole().getName(), userRes.getRole().getName());
        Assertions.assertEquals(user1.getUserProfile().getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(user1.getUserProfile().getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(user1.getUserProfile().getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenCreateUserSuccessNoFirstName_shouldReturnUser() throws BaseException {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);
        userCreateInput1.setFirstName(null);

        User userRes = userService.createUser(userCreateInput1);

        Assertions.assertEquals(user1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(user1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(user1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(user1.getRole().getName(), userRes.getRole().getName());
        Assertions.assertEquals("", userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(user1.getUserProfile().getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals(user1.getUserProfile().getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenCreateUserSuccessNoLastName_shouldReturnUser() throws BaseException {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);
        userCreateInput1.setLastName(null);

        User userRes = userService.createUser(userCreateInput1);

        Assertions.assertEquals(user1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(user1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(user1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(user1.getRole().getName(), userRes.getRole().getName());
        Assertions.assertEquals(user1.getUserProfile().getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals("", userRes.getUserProfile().getLastName());
        Assertions.assertEquals(user1.getUserProfile().getProfilePictureLink(), userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenCreateUserSuccessNoProfilePicture_shouldReturnUser() throws BaseException {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);
        userCreateInput1.setProfilePictureLink(null);

        User userRes = userService.createUser(userCreateInput1);

        Assertions.assertEquals(user1.getUsername(), userRes.getUsername());
        Assertions.assertEquals(user1.getPassword(), userRes.getPassword());
        Assertions.assertEquals(user1.getEmail(), userRes.getEmail());
        Assertions.assertEquals(user1.getRole().getName(), userRes.getRole().getName());
        Assertions.assertEquals(user1.getUserProfile().getFirstName(), userRes.getUserProfile().getFirstName());
        Assertions.assertEquals(user1.getUserProfile().getLastName(), userRes.getUserProfile().getLastName());
        Assertions.assertEquals("", userRes.getUserProfile().getProfilePictureLink());
    }

    @Test
    public void whenCreateUserUsernameExists_shouldThrowUsernameAlreadyExists() {
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);

        Assertions.assertThrows(UsernameAlreadyExistsException.class, () -> {
            userService.createUser(userCreateInput1);
        });
    }

    @Test
    public void whenCreateUserEmailExists_shouldThrowEmailAlreadyExists() {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(new User());

        Assertions.assertThrows(EmailAlreadyExistsException.class, () -> {
            userService.createUser(userCreateInput1);
        });
    }

    @Test
    public void whenCreateUserChangeToAdmin_shouldThrowBaseException() {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);
        userCreateInput1.setRole("ADMIN");

        Assertions.assertThrows(BaseException.class, () -> {
            userService.createUser(userCreateInput1);
        });
    }

    @Test
    public void whenCreateUserNoRole_shouldThrowNoRoleExistsException() {
        Mockito.when(userRepository.findUserByUsername(userCreateInput1.getUsername())).thenReturn(null);
        Mockito.when(userRepository.findUserByEmail(userCreateInput1.getEmail())).thenReturn(null);
        userCreateInput1.setRole("HUHA");

        Assertions.assertThrows(UserRoleNotExistsException.class, () -> {
            userService.createUser(userCreateInput1);
        });
    }
}
