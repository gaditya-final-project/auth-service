package com.gdaditya.authservice.logging;

public interface LogService {
    void log(String message);
    void logRequest(String ip, String path, String method);
}
