package com.gdaditya.authservice.controller;

import com.gdaditya.authservice.exception.*;
import com.gdaditya.authservice.model.dto.BaseResponse;
import com.gdaditya.authservice.model.dto.UserCreateInput;
import com.gdaditya.authservice.model.dto.UserUpdateInput;
import com.gdaditya.authservice.service.UserService;
import com.gdaditya.authservice.util.JwtTokenProvider;
import com.gdaditya.authservice.exception.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log4j2
@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/")
    public ResponseEntity<?> createUser(@RequestBody @Valid UserCreateInput req) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(userService.createUser(req)));
    }

    @GetMapping("/")
    public ResponseEntity<?> getCurrentUserInfo(@RequestHeader("Authorization") String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        return ResponseEntity.ok(new BaseResponse<>(userService.getFromToken(token)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserfromId(
            @PathVariable(value="id") Long id,
            @RequestHeader("Authorization") String token
    ) throws UserNotExistsException, UserNotFoundException, JwtTokenMalformedException, JwtTokenMissingException, UnauthorizedException {
        if (isTokenNotRole(token, "ADMIN") && isTokenNotRole(token,"SERVICE"))
            throw new UnauthorizedException();
        return ResponseEntity.ok(new BaseResponse<>(userService.getFromId(id)));
    }

    @GetMapping("/all")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> getAllUser(@RequestHeader("Authorization") String token) throws JwtTokenMalformedException, JwtTokenMissingException, UnauthorizedException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        return ResponseEntity.ok().body(new BaseResponse<>(userService.getAllUser()));
    }

    @PatchMapping("/")
    public ResponseEntity<?> updateUser(
            @RequestBody @Valid UserUpdateInput req,
            @RequestHeader("Authorization") String token) throws UserNotExistsException, UsernameAlreadyExistsException, JwtTokenMalformedException, UpdateOtherUserUnauthorizedException, JwtTokenMissingException, RoleNotExistsException, EmailAlreadyExistsException, UpdateRoleUnauthorizedException {
        return ResponseEntity.ok(new BaseResponse<>(userService.updateUser(req, token)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(
            @PathVariable(value="id") Long id,
            @RequestHeader("Authorization") String token
    ) throws UserNotExistsException, JwtTokenMalformedException, JwtTokenMissingException, UnauthorizedException {
        if (isTokenNotRole(token, "ADMIN"))
            throw new UnauthorizedException();
        userService.deleteUser(id);
        return ResponseEntity.ok(new BaseResponse<>(null));
    }

    private boolean isTokenNotRole(String token, String role) throws JwtTokenMalformedException, JwtTokenMissingException {
        return !jwtTokenProvider.getUserRole(token).equals(role);
    }
}

