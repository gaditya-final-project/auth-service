package com.gdaditya.authservice.controller;

import com.gdaditya.authservice.logging.LogService;
import com.gdaditya.authservice.model.dto.BaseResponse;
import com.gdaditya.authservice.model.dto.UserCreateInput;
import com.gdaditya.authservice.model.dto.UserLoginInput;
import com.gdaditya.authservice.exception.BaseException;
import com.gdaditya.authservice.service.AuthService;
import com.gdaditya.authservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final UserService userService;
    private final LogService logService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid UserCreateInput req, HttpServletRequest request) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(userService.createUser(req)));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid UserLoginInput req) throws BaseException {
        return ResponseEntity.ok(new BaseResponse<>(authService.generateToken(req)));
    }
}
