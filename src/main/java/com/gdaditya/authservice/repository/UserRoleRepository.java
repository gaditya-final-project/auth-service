package com.gdaditya.authservice.repository;

import com.gdaditya.authservice.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findUserRoleById(Long id);
    UserRole findUserRoleByName(String name);
}
