package com.gdaditya.authservice.service.impl;

import com.gdaditya.authservice.exception.BaseException;
import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.model.dto.TokenObject;
import com.gdaditya.authservice.model.dto.UserLoginInput;
import com.gdaditya.authservice.repository.UserRepository;
import com.gdaditya.authservice.util.JwtTokenProvider;
import com.gdaditya.authservice.exception.WrongUsernameOrPassword;
import com.gdaditya.authservice.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public TokenObject generateToken(UserLoginInput req) throws BaseException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        req.getUsername(),
                        req.getPassword()
                )
        );
        if (!authentication.isAuthenticated())
            throw new WrongUsernameOrPassword();
        User user = userRepository.findUserByUsername(req.getUsername());
        return jwtTokenProvider.generateToken(user);
    }
}
