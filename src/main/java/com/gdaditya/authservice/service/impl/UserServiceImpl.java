package com.gdaditya.authservice.service.impl;

import com.gdaditya.authservice.exception.*;
import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.model.dto.UserCreateInput;
import com.gdaditya.authservice.model.dto.UserUpdateInput;
import com.gdaditya.authservice.repository.UserProfileRepository;
import com.gdaditya.authservice.repository.UserRoleRepository;
import com.gdaditya.authservice.service.UserService;
import com.gdaditya.authservice.exception.*;
import com.gdaditya.authservice.model.UserProfile;
import com.gdaditya.authservice.model.UserRole;
import com.gdaditya.authservice.repository.UserRepository;
import com.gdaditya.authservice.util.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserProfileRepository userProfileRepository;
    private final UserRoleRepository userRoleRepository;
    private final JwtTokenProvider jwtTokenProvider;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user == null)
            throw new  WrongUsernameOrPassword();

        return user;
    }

    @Override
    public Collection<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getFromId(Long id) throws UserNotFoundException {
        User user = userRepository.findUserById(id);
        if (user == null)
            throw new UserNotFoundException();
        return user;
    }

    @Override
    public User getFromToken(String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        Long id = jwtTokenProvider.getUserId(token);
        User user = userRepository.findUserById(id);
        if (user == null)
            throw new JwtTokenMalformedException("User id in token does not exists!");
        return user;
    }

    @Override
    public User updateUser(UserUpdateInput userUpdateDto, String token) throws UserNotExistsException, UsernameAlreadyExistsException, JwtTokenMalformedException, JwtTokenMissingException, UpdateOtherUserUnauthorizedException, RoleNotExistsException, EmailAlreadyExistsException, UpdateRoleUnauthorizedException {
        String role = jwtTokenProvider.getUserRole(token);
        Long id = jwtTokenProvider.getUserId(token);
        if (userUpdateDto.getId() != null && userUpdateDto.getId() != id && !Objects.equals(role, "ADMIN"))
            throw new UpdateOtherUserUnauthorizedException();
        if (userUpdateDto.getId() == null)
            userUpdateDto.setId(id);

        User user = userRepository.findUserById(userUpdateDto.getId());
        if (user == null)
            throw new UserNotExistsException();
        user = userRepository.findUserByUsername(userUpdateDto.getUsername());
        if (user != null)
            throw new UsernameAlreadyExistsException();
        user = userRepository.findUserByEmail(userUpdateDto.getEmail());
        if (user != null)
            throw new EmailAlreadyExistsException();
        user = userRepository.findUserById(userUpdateDto.getId());
        UserProfile profile = user.getUserProfile();

        if (userUpdateDto.getUsername() != null)
            user.setUsername(userUpdateDto.getUsername());
        if (userUpdateDto.getPassword() != null)
            user.setPassword(passwordEncoder.encode(userUpdateDto.getPassword()));
        if (userUpdateDto.getEmail() != null)
            user.setEmail(userUpdateDto.getEmail());
        if (userUpdateDto.getRole() != null) {
            if (!Objects.equals(role, "ADMIN"))
                throw new UpdateRoleUnauthorizedException();
            UserRole role1 = userRoleRepository.findUserRoleByName(userUpdateDto.getRole());
            if (role1 == null)
                throw new RoleNotExistsException();
            user.setRole(role1);
        } if (userUpdateDto.getFirstName() != null)
            profile.setFirstName(userUpdateDto.getFirstName());
        if (userUpdateDto.getLastName() != null)
            profile.setLastName(userUpdateDto.getLastName());
        if (userUpdateDto.getProfilePictureLink() != null)
            profile.setProfilePictureLink(userUpdateDto.getProfilePictureLink());

        userProfileRepository.save(profile);
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) throws UserNotExistsException {
        User user = userRepository.findUserById(id);
        if (user == null)
            throw new UserNotExistsException();
        userRepository.delete(user);
    }

    @Override
    public User createUser(UserCreateInput req) throws BaseException {
        User user = userRepository.findUserByUsername(req.getUsername());
        if (user != null)
            throw new UsernameAlreadyExistsException();
        user = userRepository.findUserByEmail(req.getEmail());
        if (user != null)
            throw new EmailAlreadyExistsException();
        if (req.getRole().equals("ADMIN"))
            throw new BaseException(HttpStatus.UNAUTHORIZED, "Create admin user is not allowed!");

        UserRole userRole = userRoleRepository.findUserRoleByName(req.getRole());
        if (userRole == null)
            throw new UserRoleNotExistsException();

        user = User.builder()
                .username(req.getUsername())
                .password(passwordEncoder.encode(req.getPassword()))
                .email(req.getEmail())
                .role(userRole)
                .build();
        UserProfile userProfile = UserProfile.builder().user(user).build();
        if (req.getFirstName() != null)
            userProfile.setFirstName(req.getFirstName());
        if (req.getLastName() != null)
            userProfile.setLastName(req.getLastName());
        if (req.getProfilePictureLink() != null)
            userProfile.setProfilePictureLink(req.getProfilePictureLink());
        userProfileRepository.save(userProfile);
        user.setUserProfile(userProfile);;
        return userRepository.save(user);

    }
}

