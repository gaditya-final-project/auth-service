package com.gdaditya.authservice.service;

import com.gdaditya.authservice.exception.BaseException;
import com.gdaditya.authservice.model.dto.TokenObject;
import com.gdaditya.authservice.model.dto.UserLoginInput;

public interface AuthService {
    TokenObject generateToken(UserLoginInput req) throws BaseException;
}
