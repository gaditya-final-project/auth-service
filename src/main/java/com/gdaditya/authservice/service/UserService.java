package com.gdaditya.authservice.service;

import com.gdaditya.authservice.exception.*;
import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.model.dto.UserCreateInput;
import com.gdaditya.authservice.model.dto.UserUpdateInput;
import com.gdaditya.authservice.exception.*;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collection;

public interface UserService extends UserDetailsService {
    Collection<User> getAllUser();
    User getFromId(Long id) throws UserNotExistsException, UserNotFoundException;
    User getFromToken(String token) throws JwtTokenMalformedException, JwtTokenMissingException;
    User updateUser(UserUpdateInput userUpdateDto, String token) throws UserNotExistsException, UsernameAlreadyExistsException, JwtTokenMalformedException, JwtTokenMissingException, UpdateOtherUserUnauthorizedException, RoleNotExistsException, EmailAlreadyExistsException, UpdateRoleUnauthorizedException;
    void deleteUser(Long id) throws UserNotExistsException;
    User createUser(UserCreateInput req) throws BaseException;
}
