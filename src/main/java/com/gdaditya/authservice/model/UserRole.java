package com.gdaditya.authservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor
public class UserRole {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    protected Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    protected Date updatedAt;

    public UserRole(String name) { this.name = name; }

}
