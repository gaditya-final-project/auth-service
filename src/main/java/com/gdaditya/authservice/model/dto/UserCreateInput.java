package com.gdaditya.authservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UserCreateInput {
    @Pattern(regexp = "[A-Za-z]{6,}$",
            message = "Username must be at least 6 character long")
    private String username;
    @NotEmpty(message = "Password cannot be empty")
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",
            message = "Password must be at least eight characters, at least one letter, one number, and one special character")
    private String password;
    @NotEmpty(message = "E-Mail cannot be empty")
    @Pattern(regexp = "\\S+@\\S+\\.\\S+", message = "E-Mail is not valid")
    private String email;
    @NotNull(message = "Role cannot be empty")
    private String role;
    private String firstName;
    private String lastName;
    private String profilePictureLink;
}
