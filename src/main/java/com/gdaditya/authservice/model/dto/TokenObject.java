package com.gdaditya.authservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class TokenObject {
    String token;
    Date validUntil;
}
