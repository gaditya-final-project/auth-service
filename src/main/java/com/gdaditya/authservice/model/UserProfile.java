package com.gdaditya.authservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_id")
    private Long id;

    @MapsId
    @OneToOne(mappedBy = "userProfile")
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Column(name = "first_name")
    @Builder.Default
    private String firstName = "";

    @Column(name = "last_name")
    @Builder.Default
    private String lastName = "";

    @Column(name = "profile_picture_link")
    @Builder.Default
    private String profilePictureLink = "";

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    protected Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    protected Date updatedAt;

}

