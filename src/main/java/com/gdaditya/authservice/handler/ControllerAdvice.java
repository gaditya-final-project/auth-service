package com.gdaditya.authservice.handler;

import com.gdaditya.authservice.exception.BaseException;
import com.gdaditya.authservice.model.dto.BaseResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@RestControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex);
        if (ex instanceof HttpMessageNotReadableException && ex.getMessage().contains("Stream closed"))
            return new ResponseEntity(body, headers, status);
        else if (ex instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException) ex).getBindingResult();
            return ResponseEntity.status(status)
                    .body(new BaseResponse<>(false, status.value(), "Invalid parameters",
                            bindingResult.getFieldErrors().stream().map(fe ->{
                                Map<String, String> map = new HashMap<>();
                                map.put("field", fe.getField());
                                map.put("error_message", fe.getDefaultMessage());
                                return map;
                            })));
        } else if (ex instanceof HttpMessageNotReadableException)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new BaseResponse<>(false, HttpStatus.BAD_REQUEST.value(), "Request body failed to be parsed.", null));
        else
            return ResponseEntity.status(status.value())
                    .body(new BaseResponse<Object>(false, status.value(), ex.getMessage(), null));
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new BaseResponse<>(false, HttpStatus.NO_CONTENT.value(), "No handler found for "+ex.getRequestURL(), null));
    }

    @ExceptionHandler({BaseException.class})
    public ResponseEntity<Object> handleBaseException(BaseException ex, WebRequest request ) {
        log.warn(ex.getMessage());
        return ResponseEntity.status(ex.getStatus())
                .body(new BaseResponse<Object>(false, ex.getStatus().value(), ex.getMessage(), null));
    }

    @ExceptionHandler({InternalAuthenticationServiceException.class})
    public ResponseEntity<Object> handleAuthenticationException(InternalAuthenticationServiceException ex, WebRequest request ) {
        log.warn(ex.getMessage());
        if (ex.getCause() instanceof BaseException)
            return handleBaseException((BaseException) ex.getCause(), request);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(new BaseResponse<Object>(false, HttpStatus.UNAUTHORIZED.value(), ex.getMessage(), null));
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        log.error(ex);
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return ResponseEntity.status(status.value())
                .body(new BaseResponse<Object>(false, status.value(), ex.getMessage(), null));
    }
}
