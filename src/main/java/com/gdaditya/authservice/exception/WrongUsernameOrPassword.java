package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class WrongUsernameOrPassword extends BaseException {
    public WrongUsernameOrPassword() {
        super(HttpStatus.UNAUTHORIZED, "Wrong username or password!");
    }
}
