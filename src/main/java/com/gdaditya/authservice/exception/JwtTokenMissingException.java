package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

import javax.naming.AuthenticationException;

public class JwtTokenMissingException extends AuthenticationException {
    private static final long serialVersionUID = 1L;
    public JwtTokenMissingException(String msg) { super(msg); }
    public HttpStatus getStatus() {
        return HttpStatus.UNAUTHORIZED;
    }
}
