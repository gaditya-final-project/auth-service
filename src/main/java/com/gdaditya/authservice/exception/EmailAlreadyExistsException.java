package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class EmailAlreadyExistsException extends BaseException {
    public EmailAlreadyExistsException() {
        super(HttpStatus.BAD_REQUEST, "Email already registered!");
    }
}
