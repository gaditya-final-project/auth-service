package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UsernameAlreadyExistsException extends BaseException {
    public UsernameAlreadyExistsException() {
        super(HttpStatus.BAD_REQUEST, "Username already exists!");
    }
}
