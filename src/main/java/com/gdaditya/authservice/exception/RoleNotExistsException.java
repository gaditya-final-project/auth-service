package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class RoleNotExistsException extends BaseException {
    public RoleNotExistsException() {
        super(HttpStatus.BAD_REQUEST, "Role not exists!");
    }
}
