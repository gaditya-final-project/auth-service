package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UserRoleNotExistsException extends BaseException {
    public UserRoleNotExistsException() {
        super(HttpStatus.BAD_REQUEST, "User Role not exists!");
    }
}
