package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UpdateRoleUnauthorizedException extends BaseException {
    public UpdateRoleUnauthorizedException() {
        super(HttpStatus.UNAUTHORIZED, "Updating role is not permitted!");
    }
}
