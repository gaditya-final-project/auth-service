package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UserNotExistsException extends BaseException {
    public UserNotExistsException() {
        super(HttpStatus.BAD_REQUEST, "User not exists!");
    }
}
