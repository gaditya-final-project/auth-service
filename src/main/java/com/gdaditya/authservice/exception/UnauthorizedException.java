package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends BaseException {
    public UnauthorizedException() {
        super(HttpStatus.UNAUTHORIZED, "You are not authorized to do this operation!");
    }
}
