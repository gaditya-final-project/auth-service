package com.gdaditya.authservice.exception;

import org.springframework.http.HttpStatus;

public class UpdateOtherUserUnauthorizedException extends BaseException {
    public UpdateOtherUserUnauthorizedException() {
        super(HttpStatus.UNAUTHORIZED, "Update other user is not permitted!");
    }
}
