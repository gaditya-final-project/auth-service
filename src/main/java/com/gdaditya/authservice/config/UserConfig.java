package com.gdaditya.authservice.config;

import com.gdaditya.authservice.model.User;
import com.gdaditya.authservice.repository.UserProfileRepository;
import com.gdaditya.authservice.model.UserProfile;
import com.gdaditya.authservice.model.UserRole;
import com.gdaditya.authservice.repository.UserRepository;
import com.gdaditya.authservice.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class UserConfig implements ApplicationRunner {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserProfileRepository userProfileRepository;
    private final UserRoleRepository userRoleRepository;

    private void createUser(String username, String password, String email, UserRole role) {
        User user = User.builder()
                .username(username)
                .password(passwordEncoder.encode(password))
                .email(email)
                .role(role)
                .build();
        UserProfile userProfile = UserProfile.builder().user(user).build();
        userProfileRepository.save(userProfile);
        user.setUserProfile(userProfile);
        userRepository.save(user);
    }


    private void createUsers() {
        UserRole admin = userRoleRepository.save(new UserRole("ADMIN"));
        UserRole trainer = userRoleRepository.save(new UserRole("TRAINER"));
        UserRole learner = userRoleRepository.save(new UserRole("LEARNER"));

        createUser("test-admin", "123", "test1@gmail.com", admin);
        createUser("test-trainer", "123", "test2@gmail.com", trainer);
        createUser("test-learner", "123", "test3@gmail.com", learner);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        createUsers();
    }
}
